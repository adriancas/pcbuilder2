<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    
    public function indexAction(Request $request){
        //variable para el manager doctrine
        $em = $this->getDoctrine()->getManager();
        
        //recuperamos las marcas de procesador
        $marca_procesadores=$em->getRepository('BackendBundle:MarcaProcesador')->findAll();
        
       
        //retornamos a una vista con los datos
       return $this->render('AppBundle:Default:index.html.twig',array(
           'marcas_procesador'=>$marca_procesadores
       ));
    }
    
    public function procesadoresAction(Request $request){
//        //variable para el manager doctrine
        $em = $this->getDoctrine()->getManager();
        
        $id=$request->request->get('id_procesador');
        
       $query = $em->createQuery('SELECT p FROM BackendBundle:Procesador p '
                        . 'WHERE p.idMarcaProcesador = :id')
                ->setParameter('id',$id)
                ->getScalarResult();
        
        return new \Symfony\Component\HttpFoundation\JsonResponse($query);
        
    }
    
    public function tarjetasAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         $id=$request->request->get('id_tarjeta');
         
          $query = $em->createQuery('SELECT m FROM BackendBundle:Motherboard m '
                        . 'WHERE m.idMarcaProcesador = :id')
                ->setParameter('id',$id)
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    public function memoriasAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         $id=$request->request->get('id_ram');
         
          $query = $em->createQuery('SELECT r FROM BackendBundle:MemoriaRam r '
                        . 'WHERE r.idMotherboard = :id')
                ->setParameter('id',$id)
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    public function discosAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         $id=$request->request->get('id_disco');
         
          $query = $em->createQuery('SELECT d FROM BackendBundle:DiscoDuro d '
                        . 'WHERE d.idMotherboard = :id')
                ->setParameter('id',$id)
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    
    public function graficosAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         $id=$request->request->get('id_graficos');
         
          $query = $em->createQuery('SELECT g FROM BackendBundle:TarjetaGrafica g '
                        . 'WHERE g.idMotherboard = :id')
                ->setParameter('id',$id)
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    
    public function gabinetesAction(Request $request){
         $em = $this->getDoctrine()->getManager();
//         $id=$request->request->get('id');
         
          $query = $em->createQuery('SELECT g FROM BackendBundle:Gabinete g ')
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    
    public function fuentesAction(Request $request){
         $em = $this->getDoctrine()->getManager();
//         $id=$request->request->get('id');
         
          $query = $em->createQuery('SELECT f FROM BackendBundle:FuentePoder f ')
                ->getScalarResult();

         return new \Symfony\Component\HttpFoundation\JsonResponse($query);
    }
    
    
}
