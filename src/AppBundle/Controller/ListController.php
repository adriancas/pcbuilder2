<?php

namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller{
     public function listProcesadoresAction(Request $request){
          //variable para el manager doctrine
        $em = $this->getDoctrine()->getManager();
        
        //recuperamos las marcas de procesador
        $procesadores=$em->getRepository('BackendBundle:Procesador')->findAll();
        
       
        //retornamos a una vista con los datos
       return $this->render('AppBundle:Default:procesadores.html.twig',array(
           'procesadores'=>$procesadores
       ));
    }
}