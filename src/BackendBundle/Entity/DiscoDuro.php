<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiscoDuro
 *
 * @ORM\Table(name="disco_duro", indexes={@ORM\Index(name="id_motherboard", columns={"id_motherboard"})})
 * @ORM\Entity
 */
class DiscoDuro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=50, nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=200, nullable=true)
     */
    private $descripcion;

    /**
     * @var \Motherboard
     *
     * @ORM\ManyToOne(targetEntity="Motherboard")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_motherboard", referencedColumnName="id")
     * })
     */
    private $idMotherboard;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return DiscoDuro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return DiscoDuro
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set img
     *
     * @param string $img
     *
     * @return DiscoDuro
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return DiscoDuro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idMotherboard
     *
     * @param \BackendBundle\Entity\Motherboard $idMotherboard
     *
     * @return DiscoDuro
     */
    public function setIdMotherboard(\BackendBundle\Entity\Motherboard $idMotherboard = null)
    {
        $this->idMotherboard = $idMotherboard;

        return $this;
    }

    /**
     * Get idMotherboard
     *
     * @return \BackendBundle\Entity\Motherboard
     */
    public function getIdMotherboard()
    {
        return $this->idMotherboard;
    }
}
