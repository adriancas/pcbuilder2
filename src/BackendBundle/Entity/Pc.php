<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pc
 *
 * @ORM\Table(name="pc", indexes={@ORM\Index(name="fk_motherboard", columns={"motherboard"}), @ORM\Index(name="fk_procesador", columns={"procesador"}), @ORM\Index(name="fk_memoria_ram", columns={"memoria_ram"}), @ORM\Index(name="fk_disco_duro", columns={"disco_duro"}), @ORM\Index(name="fk_tarjeta_grafica", columns={"tarjeta_grafica"}), @ORM\Index(name="fk_gabinete", columns={"gabinete"}), @ORM\Index(name="fk_fuente_poder", columns={"fuente_poder"})})
 * @ORM\Entity
 */
class Pc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DiscoDuro
     *
     * @ORM\ManyToOne(targetEntity="DiscoDuro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disco_duro", referencedColumnName="id")
     * })
     */
    private $discoDuro;

    /**
     * @var \FuentePoder
     *
     * @ORM\ManyToOne(targetEntity="FuentePoder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fuente_poder", referencedColumnName="id")
     * })
     */
    private $fuentePoder;

    /**
     * @var \Gabinete
     *
     * @ORM\ManyToOne(targetEntity="Gabinete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gabinete", referencedColumnName="id")
     * })
     */
    private $gabinete;

    /**
     * @var \MemoriaRam
     *
     * @ORM\ManyToOne(targetEntity="MemoriaRam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="memoria_ram", referencedColumnName="id")
     * })
     */
    private $memoriaRam;

    /**
     * @var \Motherboard
     *
     * @ORM\ManyToOne(targetEntity="Motherboard")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="motherboard", referencedColumnName="id")
     * })
     */
    private $motherboard;

    /**
     * @var \Procesador
     *
     * @ORM\ManyToOne(targetEntity="Procesador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="procesador", referencedColumnName="id")
     * })
     */
    private $procesador;

    /**
     * @var \TarjetaGrafica
     *
     * @ORM\ManyToOne(targetEntity="TarjetaGrafica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarjeta_grafica", referencedColumnName="id")
     * })
     */
    private $tarjetaGrafica;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discoDuro
     *
     * @param \BackendBundle\Entity\DiscoDuro $discoDuro
     *
     * @return Pc
     */
    public function setDiscoDuro(\BackendBundle\Entity\DiscoDuro $discoDuro = null)
    {
        $this->discoDuro = $discoDuro;

        return $this;
    }

    /**
     * Get discoDuro
     *
     * @return \BackendBundle\Entity\DiscoDuro
     */
    public function getDiscoDuro()
    {
        return $this->discoDuro;
    }

    /**
     * Set fuentePoder
     *
     * @param \BackendBundle\Entity\FuentePoder $fuentePoder
     *
     * @return Pc
     */
    public function setFuentePoder(\BackendBundle\Entity\FuentePoder $fuentePoder = null)
    {
        $this->fuentePoder = $fuentePoder;

        return $this;
    }

    /**
     * Get fuentePoder
     *
     * @return \BackendBundle\Entity\FuentePoder
     */
    public function getFuentePoder()
    {
        return $this->fuentePoder;
    }

    /**
     * Set gabinete
     *
     * @param \BackendBundle\Entity\Gabinete $gabinete
     *
     * @return Pc
     */
    public function setGabinete(\BackendBundle\Entity\Gabinete $gabinete = null)
    {
        $this->gabinete = $gabinete;

        return $this;
    }

    /**
     * Get gabinete
     *
     * @return \BackendBundle\Entity\Gabinete
     */
    public function getGabinete()
    {
        return $this->gabinete;
    }

    /**
     * Set memoriaRam
     *
     * @param \BackendBundle\Entity\MemoriaRam $memoriaRam
     *
     * @return Pc
     */
    public function setMemoriaRam(\BackendBundle\Entity\MemoriaRam $memoriaRam = null)
    {
        $this->memoriaRam = $memoriaRam;

        return $this;
    }

    /**
     * Get memoriaRam
     *
     * @return \BackendBundle\Entity\MemoriaRam
     */
    public function getMemoriaRam()
    {
        return $this->memoriaRam;
    }

    /**
     * Set motherboard
     *
     * @param \BackendBundle\Entity\Motherboard $motherboard
     *
     * @return Pc
     */
    public function setMotherboard(\BackendBundle\Entity\Motherboard $motherboard = null)
    {
        $this->motherboard = $motherboard;

        return $this;
    }

    /**
     * Get motherboard
     *
     * @return \BackendBundle\Entity\Motherboard
     */
    public function getMotherboard()
    {
        return $this->motherboard;
    }

    /**
     * Set procesador
     *
     * @param \BackendBundle\Entity\Procesador $procesador
     *
     * @return Pc
     */
    public function setProcesador(\BackendBundle\Entity\Procesador $procesador = null)
    {
        $this->procesador = $procesador;

        return $this;
    }

    /**
     * Get procesador
     *
     * @return \BackendBundle\Entity\Procesador
     */
    public function getProcesador()
    {
        return $this->procesador;
    }

    /**
     * Set tarjetaGrafica
     *
     * @param \BackendBundle\Entity\TarjetaGrafica $tarjetaGrafica
     *
     * @return Pc
     */
    public function setTarjetaGrafica(\BackendBundle\Entity\TarjetaGrafica $tarjetaGrafica = null)
    {
        $this->tarjetaGrafica = $tarjetaGrafica;

        return $this;
    }

    /**
     * Get tarjetaGrafica
     *
     * @return \BackendBundle\Entity\TarjetaGrafica
     */
    public function getTarjetaGrafica()
    {
        return $this->tarjetaGrafica;
    }
}
