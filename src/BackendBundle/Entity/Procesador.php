<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procesador
 *
 * @ORM\Table(name="procesador", indexes={@ORM\Index(name="id_marca_procesador", columns={"id_marca_procesador"})})
 * @ORM\Entity
 */
class Procesador
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=50, nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=200, nullable=true)
     */
    private $descripcion;

    /**
     * @var \MarcaProcesador
     *
     * @ORM\ManyToOne(targetEntity="MarcaProcesador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca_procesador", referencedColumnName="id")
     * })
     */
    private $idMarcaProcesador;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Procesador
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Procesador
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set img
     *
     * @param string $img
     *
     * @return Procesador
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Procesador
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idMarcaProcesador
     *
     * @param \BackendBundle\Entity\MarcaProcesador $idMarcaProcesador
     *
     * @return Procesador
     */
    public function setIdMarcaProcesador(\BackendBundle\Entity\MarcaProcesador $idMarcaProcesador = null)
    {
        $this->idMarcaProcesador = $idMarcaProcesador;

        return $this;
    }

    /**
     * Get idMarcaProcesador
     *
     * @return \BackendBundle\Entity\MarcaProcesador
     */
    public function getIdMarcaProcesador()
    {
        return $this->idMarcaProcesador;
    }
}
